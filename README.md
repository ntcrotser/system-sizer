# System Sizer

======================

System Sizer is a quick reference program for generating the true capacity of a water softener under problem water conditions. System Sizer will also factor in a 10% system reserve automatically.

System Sizer's source code is available under the MIT License and is provided AS-IS with NO WARRANTIES of ANY KIND! 
For more information, consult LICENSE.txt that accompanied this program.

Don't be Bogus!

System Sizer © 2019-2022 N.T. Crotser: ntc@crotsertech.co
